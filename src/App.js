import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css";
import { Container } from 'react-bootstrap'
import {Navigate, Route, Routes} from "react-router-dom";
import HomeScreen from "./screens/HomeScreen";
import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import HelpScreen from "./screens/HelpScreen";
import ProfileScreen from "./screens/ProfileScreen";
import ProfileSetting from "./components/ProfileSetting/ProfileSetting";
import Family from "./components/Family/Family";
import Admin from "./components/Admin/Admin";
import AdminScreen from "./screens/AdminScreen";

function App() {
  return (
      <div className="App">
          <main>
              <Container>
                  <Routes>
                      <Route path="/" element={<HomeScreen />} />
                      <Route path="login" element={<LoginScreen />} />
                      <Route path="help" element={<HelpScreen />} />
                      <Route path="register" element={<RegisterScreen />} />
                      <Route path="profile" element={<ProfileScreen />} />
                      <Route path="settings" element={<ProfileSetting />} />
                      <Route path="score" element={<Family />} />
                      <Route path="admin" element={<AdminScreen />} />
                      <Route path="*" element={<Navigate to='/' replace />} />
                  </Routes>

                  {/* <HomeScreen /> */}

              </Container>
          </main>
      </div>
    );
}

export default App;
