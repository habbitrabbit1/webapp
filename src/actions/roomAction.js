import {
    BASE_ENDPOINT, CONFIRM_ROOM_FAIL, CONFIRM_ROOM_REQUEST, CONFIRM_ROOM_SUCCESS,
    USER_LOGIN_SUCCESS, USER_REGISTER_FAIL,
    USER_REGISTER_REQUEST,
    USER_REGISTER_SUCCESS
} from "../constants/userConstants";
import axios from "axios";
import {
    DELETE_ROOM_FAIL, DELETE_ROOM_REQUEST,
    DELETE_ROOM_SUCCESS,
    FETCH_ROOM_FAIL,
    FETCH_ROOM_REQUEST,
    FETCH_ROOM_SUCCESS
} from "../constants/roomConstants";

export const createRoom = (name, description, creatorId, finishedAt, rabbitsForFailure, rabbitsForSuccess, emails, jwt) => async (dispatch) => {
    try {
        dispatch({
            type: USER_REGISTER_REQUEST
        })

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': jwt,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With'
            }
        }

        const {data} = await axios.post(
            BASE_ENDPOINT + '/rooms/create',
            {
                'name': name,
                'description': description,
                'creatorId': creatorId,
                'finishedAt': finishedAt,
                'rabbitsForFailure': rabbitsForFailure,
                'rabbitsForSuccess': rabbitsForSuccess,
                'emails': emails
            },
            config
        )

    } catch (error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: "Ошибка регистрации",
        })
    }
}

export const fetchRoom = (roomId, jwt) => async (dispatch) => {
    try {
        dispatch({
            type: FETCH_ROOM_REQUEST
        })

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': jwt,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With'
            }
        }

        const {data} = await axios.post(
            BASE_ENDPOINT + '/rooms/get_rooms/' + roomId,
            null,
            config
        )

        console.log(data)

        dispatch({
            type: FETCH_ROOM_SUCCESS,
            payload: data,
        })

    } catch (error) {
        dispatch({
            type: FETCH_ROOM_FAIL,
            payload: "Ошбика фетча",
        })
    }
}

export const confirmRoom = (roomId, playerId, jwt) => async (dispatch) => {
    try {
        dispatch({
            type: CONFIRM_ROOM_REQUEST
        })

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': jwt,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With'
            }
        }

        const {data} = await axios.post(
            BASE_ENDPOINT + '/confirmation',
            {
                'roomId': roomId,
                'playerId': playerId,
            },
            config
        )

        console.log(data)

        dispatch({
            type: CONFIRM_ROOM_SUCCESS,
            payload: data,
        })

    } catch (error) {
        dispatch({
            type: CONFIRM_ROOM_FAIL,
            payload: "Ошбика фетча",
        })
    }
}

export const deleteRoom = (roomId, jwt) => async (dispatch) => {
    try {
        dispatch({
            type: DELETE_ROOM_REQUEST
        })

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': jwt,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'DELETE, POST, GET, OPTIONS',
                'Access-Control-Allow-Headers': 'Content-Type, Authorization, X-Requested-With'
            }
        }

        const {data} = await axios.post(
            BASE_ENDPOINT + '/rooms/delete',
            {
                'roomId': roomId,
            },
            config
        )

        console.log(data)

        dispatch({
            type: DELETE_ROOM_SUCCESS,
            payload: data,
        })

    } catch (error) {
        dispatch({
            type: DELETE_ROOM_FAIL,
            payload: "Ошбика фетча",
        })
    }
}
