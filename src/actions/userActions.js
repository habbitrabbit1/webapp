import axios from 'axios'
import {
    USER_LOGIN_REQUEST,
    USER_LOGIN_SUCCESS,
    USER_LOGIN_FAIL,
    USER_LOGOUT,
    USER_REGISTER_REQUEST,
    USER_REGISTER_SUCCESS,
    USER_REGISTER_FAIL, BASE_ENDPOINT, USER_UPDATE_REQUEST, USER_UPDATE_SUCCESS, USER_UPDATE_FAIL
} from '../constants/userConstants'


export const login = (email, password) => async (dispatch) => {
    try {
        dispatch({
            type: USER_LOGIN_REQUEST
        })

        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const {data} = await axios.post(
            BASE_ENDPOINT + '/auth',
            {'email': email, 'password': password},
            config
        )

        const money = Math.floor((Math.random()*10)+1);

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: {...data, money}
        })

        localStorage.setItem('userInfo', JSON.stringify(data))

    } catch (error) {
        dispatch({
            type: USER_LOGIN_FAIL,
            payload: "Ошибка авторизация",
        })
    }
}


export const logout = () => (dispatch) => {
    localStorage.removeItem('userInfo')
    dispatch({type: USER_LOGOUT})
}


export const register = (name, email, password) => async (dispatch) => {
    try {
        dispatch({
            type: USER_REGISTER_REQUEST
        })

        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const {data} = await axios.post(
            BASE_ENDPOINT + '/players/register',
            {'name': name, 'email': email, 'password': password, isAdmin: false},
            config
        )

        const money = Math.floor((Math.random()*10)+1);

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: {...data, money}
        })

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: data
        })

        localStorage.setItem('userInfo', JSON.stringify(data))

    } catch (error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: "Ошибка регистрации",
        })
    }
}

export const update = (name, password, jwt) => async (dispatch) => {
    try {
        dispatch({
            type: USER_UPDATE_REQUEST
        })

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': jwt
            }
        }

        const {data} = await axios.post(
            BASE_ENDPOINT + '/players/change',
            {'name': name, 'password': password},
            config
        )

        dispatch({
            type: USER_UPDATE_SUCCESS,
            payload: {'name': data['name'], 'email': data['email'], 'jwt': jwt}
        })

        localStorage.setItem('userInfo', JSON.stringify(data))

    } catch (error) {
        dispatch({
            type: USER_UPDATE_FAIL,
            payload: "Не удалось обновить пользователя",
        })
    }
}

