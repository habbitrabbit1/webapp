import {useSelector} from "react-redux";
import {BrowserRouter, NavLink, useNavigate} from "react-router-dom";
import {Button, Card, CardGroup, Col, Container, Modal, Nav, NavDropdown, Row} from "react-bootstrap";
import React, { useState, useEffect } from 'react';
import AdminPicture from "../../assets/img/admin.jpg";
import Room from "../Room/Room";
import RoomCreation from "../RoomCreation/RoomCreation";
import "./Admin.css";
import User from "../User/User";


export default function Admin() {
    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin
    const [rooms, setRooms] = useState(false);
    const Admin = true;

    let navigate = useNavigate();
    const handleRooms = () => setRooms(true);
    const handleUsers = () => setRooms(false);

    useEffect(() => {
        if(!userInfo){
            return navigate('/login');
        }
    }, [userInfo, navigate])

    useEffect(() => {
        if(!Admin){
            return navigate('/profile');
        }
    }, [userInfo, navigate])


    function getName() {
        return userInfo != null ? userInfo.name : ""
    }

    let players = [{
        id: 1,
        name: "Lida",
        email: "lidochka@gmail.com",
        password: "12345",
        isActive: true,
        updatedAt: "6/2/2022",
        createdAt: "6/14/2022",
        countOfRabbits: 1
    },
        {
            id: 2,
            name: "Petr",
            email: "petr@gmail.com",
            password: "12345",
            isActive: true,
            updatedAt: "6/1/2022",
            createdAt: "6/14/2022",
            countOfRabbits: 100
        }];


    const item = players[0];



    function getItemByObject(players) {
        return {
            name: players.name,
            email: players.email,
            createdAt: players.createdAt,
            countOfRabbits: players.countOfRabbits
        };
    }



    return (
        <>
            <Container fluid>
                <Row>
                    <div className= "profile-colored">
                        <div className="profile-pp">
                            <NavLink to="/" className="d-flex justify-content-center mx-auto col-md-4">
                                <img src={AdminPicture}
                                     className="rounded-circle float-end profile-picture-img profile-avatar mt-3"
                                     width="110"
                                     height="110"
                                     border={"3"}
                                     alt="Profile"/>
                            </NavLink>
                        </div>
                        <h3 className="px-3 user-name d-flex justify-content-center mx-auto col-md-4" style={{color: "#0B4654"}}> ADMIN: @{getName()}</h3>
                        <p className="px-3 user-name d-flex justify-content-center mx-auto col-md-4 fs-6 fw-light" style={{color: "#0B4654"}}> Member since April 01, 2022</p>
                        <div className="justify-content-right">
                            <RoomCreation/>
                        </div>
                    </div>

                    <Card>
                        <Card.Header>
                            {
                                /*<Nav className="justify-content-center" defaultActiveKey="#first">
                                    <Nav.Item>
                                        <a style={{ cursor: 'pointer' }} onClick={handleUsers}>
                                        <Nav.Link id="nav-active-tab" color={"#a0a0a0"} data-toggle="tab" href="#Active" role="tab" aria-controls="nav-active">Users</Nav.Link>
                                        </a>
                                    </Nav.Item>
                                    <Nav.Item >
                                        <a style={{ cursor: 'pointer' }} onClick={handleRooms}>
                                            <Nav.Link id="nav-completed-tab" data-toggle="tab" href="#Completed" role="tab" aria-controls="nav-completed">Rooms</Nav.Link>
                                        </a>
                                        </Nav.Item>
                                </Nav>*/}
                        </Card.Header>
                        <Card.Body>
                            <CardGroup>
                                {players.map((player) =>
                                    <User item={getItemByObject(player)} />
                                )}

                            </CardGroup>
                            {/* { (!rooms) ? (<>
                                <CardGroup>
                                    <User/>
                                    <User/>
                                    <User/>
                                </CardGroup>
                                <>
                                </>
                            </>):(<>
                                <CardGroup>
                                    <Room/>
                                    <Room/>
                                    <Room/>
                                </CardGroup>
                            </>)
                            }*/}
                        </Card.Body>
                    </Card>
                </Row>
            </Container>
        </>
        /*
        <div className="container">
            <header className="jumbotron">
                <h3>
                    <strong>{currentUser.username}</strong> Profile
                </h3>
            </header>
            <p>
                <strong>Token:</strong> {currentUser.accessToken.substring(0, 20)} ...{" "}
                {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
            </p>
            <p>
                <strong>Id:</strong> {currentUser.id}
            </p>
            <p>
                <strong>Email:</strong> {currentUser.email}
            </p>
            <strong>Authorities:</strong>
            {/!*<ul>
                {currentUser.roles &&
                    currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
            </ul>*!/}
        </div>*/
    );

}

