import React from 'react';
import {Col, Container} from 'react-bootstrap';
import bgImg from "../../assets/img/bg.png"

export default function Background() {
    return (
        <>
            <Container fluid className="main-back">
                <Col lg={8} md={6} sm={12}>
                    <img className="w-100 h-100 position-absolute" src={bgImg} alt=""/>
                </Col>
            </Container>
        </>
      );
}
