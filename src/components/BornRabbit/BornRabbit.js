import {Button, Card, Modal, Toast, ToastContainer} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import React, {useState, useEffect} from 'react';
import {useDispatch} from "react-redux";
import {confirmRoom} from "../../actions/roomAction";

export default function BornRabbit(props) {

    const {playerId, id, isClicked, jwt} = props.item

    const [show, setShow] = useState(false);

    const [showA, setShowA] = useState(false);
    const dispatch = useDispatch()

    function handleJmak() {
        dispatch(confirmRoom(id, playerId, jwt))
        toggleShowA()
    }

    const toggleShowA = () => setShowA(true);
    const toggleHideA = () => setShowA(false);

    return (
        <div>

            {!isClicked ?
                <Button variant="primary" onClick={handleJmak}>
                    Oh Great! (*˘︶˘*)
                </Button> : <Button variant="primary" onClose={toggleHideA}>
                    Today, you already clicked!
                </Button>
            }
            <ToastContainer className="p-3" position={"middle-end"}>
                <Toast show={showA} onClose={toggleHideA}>
                    <Toast.Header>
                        <img
                            src="holder.js/20x20?text=%20"
                            className="rounded me-2"
                            alt=""
                        />
                        <strong className="me-auto">A rabbit was born in your family!</strong>
                        <small>Right now</small>
                    </Toast.Header>
                    <Toast.Body> 🐰 Woohoo!</Toast.Body>
                </Toast>
            </ToastContainer>
        </div>
    );
}
