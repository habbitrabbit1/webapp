import {Button, Card, Modal, Nav} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import React, { useState, useEffect } from 'react';
import {useSelector} from "react-redux";

export default function Family(props){
    const [show, setShow] = useState(false);
    /*const userLogin = useSelector(state => state.user)
    const { userInfo } = userLogin*/

/*
    function getScore() {
        return userInfo != null ? userInfo.name : ""
    }
*/

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return(
        <div>
                <a style={{ cursor: 'pointer' }} onClick={handleShow}>
                    <Nav.Link  style={{color: "#0B4654"}}>Family</Nav.Link>
                    </a>


            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Score</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Now there are {props.money} rabbits in your family!
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>
                        Understand
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
