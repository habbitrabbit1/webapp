import {Col, Container, Figure, Row} from 'react-bootstrap';
import "./HomeContent.css"
import boyImg from "../../assets/img/boy.png";
import {useSelector} from "react-redux";
import React, { useState, useEffect } from 'react';
import {useNavigate} from "react-router-dom";

export default function HomeContent() {
    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

   function getName() {
        return userInfo != null ? userInfo.name : ""
    }

    let navigate = useNavigate();

    useEffect(() => {
        if(userInfo){
            return navigate('/profile');
        }
    }, [userInfo, navigate])


    return(
        <>
            <Container fluid>
                <Row>
                    <Col>
                        {console.log(userInfo)}
                        <div className="all-home-page-text">
                            <h1 className="fw-bolder px-3 fw-bolder fs-1" style={{color: "#0B4654"}}>Hey, {getName()}</h1>
                            <h1 className="fw-bolder px-3 fw-bolder fs-1" style={{color: "#0B4654"}}>I'm a good habits
                                tracker</h1>
                            <h1 className="py-3"></h1>
                            <div className="home-page-text">
                                <h3 className="px-3 fs-4" style={{color: "#0B4654"}}>I will help you develop new healthy
                                    habits.</h3>
                                <h3 className="px-3 fs-4" style={{color: "#0B4654"}}>You can team up with your friends</h3>
                                <h3 className="px-3 fs-4" style={{color: "#0B4654"}}>and breed rabbits together!</h3>
                            </div>
                        </div>
                    </Col>
                    <Col>
                        <img src={boyImg}
                             className="rounded float-end boy-img"
                             width="680"
                             height="680"
                             alt="..."/>
                    </Col>
                </Row>
            </Container>

        </>
    )
}
