import React, { useState, useEffect } from 'react'
import {Button, Container, Col, Row, Form } from "react-bootstrap";
import "./Login.css"
import {useDispatch, useSelector} from "react-redux";
import {Link, useNavigate} from "react-router-dom";
import {login} from "../../actions/userActions";

const required = (value) => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

const Login = ({ location, history }) =>  {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [ errors, setErrors ] = useState({})

    const dispatch = useDispatch()

    const userLogin = useSelector(state => state.userLogin)

    const { userInfo } = userLogin

    let navigate = useNavigate();

    useEffect(() => {
        if(userInfo){
            return navigate('/profile');
        }
    }, [userInfo, navigate])


    const submitHandler = (event) => {
        event.preventDefault()
        dispatch(login(email, password))
    }

    return(
    <>
    <Container className="mt-5">
        <Row>
            <Col lg={4} md={6} sm={12}>
                <Form className="main-login-form" onSubmit={submitHandler}>
                    <Form.Group controlId="formBasicEmail" className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control className="form-control text-indent shadow-sm bg-grey-light border-1 rounded-pill fw-lighter fs-7 p-3"
                                      type="email"
                                      placeholder="Enter email"
                                      value={email}
                                      onChange={(event) => setEmail(event.target.value)}
                                      validations={[required]}
                        />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword" className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control className="form-control text-indent shadow-sm bg-grey-light border-1 rounded-pill fw-lighter fs-7 p-3"
                                      type="password"
                                      placeholder="Enter password"
                                      value={password}
                                      onChange={(event) => setPassword(event.target.value)}
                                      isInvalid={ errors.name }
                        />
                        <Form.Control.Feedback type='invalid'>
                            { errors.name }
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label="I'm not a robot (as far as i know)"/>
                    </Form.Group>
                    <Form.Group controlId="formBasicRegister">
                    <div className="mb-3">
                    <Form.Text className="text">
                        Don't have an account?
                        </Form.Text>
                    <Link to={'/register'} className="px-1 py-5" >sign up</Link>
                    </div>
                    </Form.Group>
                    <div className="d-grid gap-2 text-center">
                        <Button className="btn-outline-dark rounded-pill mt-4 w-100" variant="light btn-block" type="submit">Submit</Button>
                    </div>
                </Form>
            </Col>

        </Row>
</Container>
    </>
);
};

export default Login;

