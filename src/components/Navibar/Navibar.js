import React from 'react';
import {Navbar, Nav, Link, Container, Button} from 'react-bootstrap';
import {useDispatch, useSelector} from 'react-redux'
import "./Navibar.css"
import {LinkContainer} from 'react-router-bootstrap'
import iconImg from "../../assets/img/icon.png"
import {logout} from "../../actions/userActions";
import * as PropTypes from "prop-types";
import ProfileSetting from "../ProfileSetting/ProfileSetting";
import Family from "../Family/Family";


LinkContainer.propTypes = {
    to: PropTypes.string,
    children: PropTypes.node
};

export default function Navibar() {
    const userLogin = useSelector(state => state.userLogin)

    const {userInfo} = userLogin

    const dispatch = useDispatch()

    function login() {
        return userInfo != null ? userInfo.name : "Login"
    }

    function handleLogout() {
        console.log("was")
        dispatch(logout())
    }

    return (
        <>
            <Navbar collapseOnSelect expand="lg" className="navbar-main" bg-transparent="true">
                <LinkContainer to='/'>
                    <Navbar.Brand className="px-5 fw-bolder fs-2 brand-name">
                        <img
                            alt=""
                            src={iconImg}
                            width="85"
                            height="70"
                            className="px-2 d-inline-block align-middle"
                        />{' '}
                        HabitRabbit
                    </Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Container className="px-2">
                    <Navbar.Collapse className="justify-content-end">
                        {
                            userInfo != null ? (
                                <Nav className="mr-auto">
                                    <LinkContainer to='/profile'>
                                        <Nav.Link to="/profile" style={{color: "#0B4654"}}>Profile</Nav.Link>
                                    </LinkContainer>
                                    <Family money={userInfo.money}/>
                                    <ProfileSetting/>
                                    {/*<LinkContainer to='/settings'>
                                <Nav.Link to="/settings" style={{color: "#0B4654"}}>Settings</Nav.Link>
                            </LinkContainer>*/}
                                    <Button className="btn-outline-dark rounded-pill mt-1 mx-3 w-100"
                                            variant="light btn-block"
                                            type="submit"
                                            onClick={handleLogout}>Logout</Button>
                                </Nav>
                            ) : (
                                <Nav className="mr-auto">
                                    <LinkContainer to='/help'>
                                        <Nav.Link to="/help" style={{color: "#0B4654"}}>Help</Nav.Link>
                                    </LinkContainer>
                                    <Nav.Link to='/' style={{color: "#0B4654"}}>About</Nav.Link>
                                    <LinkContainer to='/'>
                                        <Nav.Link to='/' style={{color: "#0B4654"}}>Contact</Nav.Link>
                                    </LinkContainer>
                                    <LinkContainer to='/login'>
                                        <Nav.Link to="/login" style={{color: "#0B4654"}}>{login()}</Nav.Link>
                                    </LinkContainer>
                                    {/*{*/}
                                    {/*    dispatch(logout())}*/}

                                </Nav>
                            )
                        }

                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}
