import {useDispatch, useSelector} from "react-redux";
import {BrowserRouter, NavLink, useNavigate} from "react-router-dom";
import {Button, Card, CardGroup, Col, Container, Modal, Nav, NavDropdown, Row} from "react-bootstrap";
import React, {useState, useEffect} from 'react';
import ProfilePicture from "../../assets/img/avatar.jpg";
import Room from "../Room/Room";
import RoomCreation from "../RoomCreation/RoomCreation";
import {fetchRoom} from "../../actions/roomAction";

import "./Profile.css";


export default function Profile() {
    const userLogin = useSelector(state => state.userLogin)
    const {userInfo} = userLogin
    const dispatch = useDispatch()

    const roomState = useSelector(state => state.roomFetch)

    let navigate = useNavigate();

    function getName() {
        return userInfo != null ? userInfo.name : ""
    }

    function fetchAllRooms() {
        dispatch(fetchRoom(userInfo.id, userInfo.jwt))
    }

    function getRoomProps(room) {
        return {
            userIsAdmin: userInfo.id === room.creatorId,
            name: room.name,
            id: room.id,
            active:room.active,
            isClicked: room.isClicked,
            createdAt:room.createdAt,
            finishedAt:room.finishedAt,
            description: room.description,
            rabbitsForSuccess: room.rabbitsForFailure,
            rabbitsForFailure: room.rabbitsForFailure
        }
    }

    return (
        <>
            <Container fluid>
                {console.log(userInfo)}
                <Row>
                    <div className="profile-colored">
                        <div className="profile-pp">
                            <NavLink to="/settings" className="d-flex justify-content-center mx-auto col-md-4">
                                <img src={ProfilePicture}
                                     className="rounded-circle float-end profile-picture-img profile-avatar mt-3"
                                     width="110"
                                     height="110"
                                     border={"3"}
                                     alt="Profile"/>
                            </NavLink>
                        </div>
                        <h3 className="px-3 user-name d-flex justify-content-center mx-auto col-md-4"
                            style={{color: "#0B4654"}}> @{getName()}</h3>
                        <p className="px-3 user-name d-flex justify-content-center mx-auto col-md-4 fs-6 fw-light"
                           style={{color: "#0B4654"}}> Member since April 01, 2022</p>
                        <div className="justify-content-right">
                            <RoomCreation/>
                        </div>
                    </div>

                    <Card>
                        <Card.Header>
                            {/*
                            <Nav className="justify-content-center" defaultActiveKey="#first">
                                <Nav.Item>
                                    <Nav.Link id="nav-active-tab" color={"#a0a0a0"} data-toggle="tab" href="#Active" role="tab" aria-controls="nav-active">Active</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link id="nav-completed-tab" data-toggle="tab" href="#Completed" role="tab" aria-controls="nav-completed">Completed</Nav.Link>
                                </Nav.Item>
                            </Nav>*/}
                            <button onClick={fetchAllRooms}>Get my rooms</button>
                        </Card.Header>
                        <Card.Body>
                            {<>
                                <CardGroup>
                                    {console.log(roomState)}
                                    {roomState.rooms !== undefined? roomState.rooms.map((room) =>
                                        <Room item={getRoomProps(room)}/>
                                    ):<div></div>}
                                </CardGroup>

                                <>
                                </>
                            </>
                            }
                        </Card.Body>
                    </Card>
                </Row>
            </Container>
        </>
        /*
        <div className="container">
            <header className="jumbotron">
                <h3>
                    <strong>{currentUser.username}</strong> Profile
                </h3>
            </header>
            <p>
                <strong>Token:</strong> {currentUser.accessToken.substring(0, 20)} ...{" "}
                {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}
            </p>
            <p>
                <strong>Id:</strong> {currentUser.id}
            </p>
            <p>
                <strong>Email:</strong> {currentUser.email}
            </p>
            <strong>Authorities:</strong>
            {/!*<ul>
                {currentUser.roles &&
                    currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
            </ul>*!/}
        </div>*/
    );

}
