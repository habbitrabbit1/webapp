import {useDispatch, useSelector} from "react-redux";
import {BrowserRouter, NavLink, useNavigate} from "react-router-dom";
import {Button, Card, CardGroup, Col, Container, Form, Modal, Nav, NavDropdown, Row} from "react-bootstrap";
import React, {useState, useEffect} from 'react';
import ProfilePicture from "../../assets/img/avatar.jpg";
import {login, update} from "../../actions/userActions";


export default function ProfileSetting(props) {
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')

    const [show, setShow] = useState(false);
    const userLogin = useSelector(state => state.userLogin)
    const {userInfo} = userLogin
    const dispatch = useDispatch()


    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function handleSave(event) {
        if (password != confirmPassword) {
            alert("Пароли не совпадают!");

            return;
        }

        setShow(false);
        event.preventDefault();
        dispatch(update(name, password, userInfo.jwt))
    }


    return (
        <div>
            <Button className="rounded-pill mt-1 w-40 mx-1"
                    variant="outline-dark"
                    type="submit"
                    onClick={handleShow}
            >Setting</Button>

            <Modal show={show} onHide={handleClose}
                   {...props}
                   size="lg"
                   aria-labelledby="contained-modal-title-vcenter"
                   centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>Profile setting</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder={userInfo.name}
                                autoFocus
                                value={name}
                                onChange={(event) => setName(event.target.value)}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="pass"
                                placeholder="Type new password"
                                autoFocus
                                value={password}
                                onChange={(event) => setPassword(event.target.value)}

                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                                type="pass"
                                placeholder="Confirm your password"
                                autoFocus
                                value={confirmPassword}
                                onChange={(event) => setConfirmPassword(event.target.value)}

                            />
                        </Form.Group>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Back
                    </Button>
                    <Button variant="primary" onClick={handleSave}>
                        Save
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>

    );

}
