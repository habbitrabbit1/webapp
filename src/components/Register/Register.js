import React, { useState, useEffect } from 'react'
import {Button, Container, Col, Row, Form } from "react-bootstrap";
import "./Register.css"
import {useDispatch, useSelector} from "react-redux";
import {Link, useNavigate} from "react-router-dom";
import {register} from "../../actions/userActions";

const Register = ({ location, history }) => {

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [message, setMessage] = useState('')

    const dispatch = useDispatch()


    const userRegister = useSelector(state => state.userRegister)
    const { userInfo } = userRegister

    let navigate = useNavigate();

    useEffect(() => {
        if(userInfo){
            return navigate("/");
        }
    }, [userInfo, navigate])


    const submitHandler = (event) => {
        event.preventDefault()
        dispatch(register(name, email, password))
    }

    return(
    <>
    <Container className="mt-5">
        <Row>
            <Col lg={4} md={6} sm={12}>
                <Form className="main-register-form" onSubmit={submitHandler}>
                    {/* <Form> */}
                    <Form.Group controlId="formBasicUsername" className="mb-3">
                        <Form.Label>Username</Form.Label>
                        <Form.Control className="form-control text-indent shadow-sm bg-grey-light border-1 rounded-pill fw-lighter fs-7 p-3"
                                      type="text"
                                      placeholder="Enter username"
                                      value={name}
                                      onChange={(event => setName(event.target.value))}
                        />

                    </Form.Group>

                    <Form.Group controlId="formBasicEmail" className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control className="form-control text-indent shadow-sm bg-grey-light border-1 rounded-pill fw-lighter fs-7 p-3"
                                      type="email"
                                      placeholder="Enter email"
                                      value={email}
                                      onChange={(event) => setEmail(event.target.value)}
                        />

                    </Form.Group>

                    <Form.Group controlId="formBasicPassword" className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control className="form-control text-indent shadow-sm bg-grey-light border-1 rounded-pill fw-lighter fs-7 p-3"
                                      type="password"
                                      placeholder="Enter password"
                                      value={password}
                                      onChange={(event) => setPassword(event.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label="I agree with the terms of the user agreement"/>
                    </Form.Group>
                    <Form.Group controlId="formBasicRegister">
                    <div className="mb-3">
                    <Form.Text className="text">
                    Already have a page?
                        </Form.Text>
                        <Link to={'/login'}>Sign in here.</Link>
                    </div>
                    </Form.Group>
                    <div className="d-grid gap-2 text-center">
                        <Button className="btn-outline-dark rounded-pill mt-4 w-100" variant="light btn-block" type="submit">Register</Button>
                    </div>
                </Form>
            </Col>

        </Row>
</Container>
    </>
);
};

export default Register;
