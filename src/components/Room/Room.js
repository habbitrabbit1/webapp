import {Button, Card, Modal} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import React, {useState, useEffect} from 'react';
import ProfilePicture from "../../assets/img/avatar.jpg";
import familyPicture from "../../assets/img/family.jpg";
import {useDispatch, useSelector} from "react-redux";
import BornRabbit from "../BornRabbit/BornRabbit";
import {deleteRoom} from "../../actions/roomAction";

export default function Room(props) {
    const [show, setShow] = useState(false);

    const userLogin = useSelector(state => state.userLogin)
    const {userInfo} = userLogin

    const dispatch = useDispatch()
    /*const userAdmin = useSelector(state => state.userLogin);*/
    const {
        userIsAdmin,
        name,
        id,
        createdAt,
        finishedAt,
        description,
        isClicked,
        active,
        rabbitsForSuccess,
        rabbitsForFailure
    } = props.item;
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function handleRemoveRoom() {
        console.log(id)
        dispatch(deleteRoom(id, userInfo.jwt))
        handleClose()
    }

    return (
        <div>
            <Card border="secondary" className="mx-1" style={{width: '18rem'}}>
                <a style={{cursor: 'pointer'}} onClick={handleShow}>
                    <Card.Header></Card.Header>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Text>
                            <div>Your lost {rabbitsForSuccess}</div>
                            <div>Your get {rabbitsForFailure}</div>
                            s
                            {description}
                        </Card.Text>
                    </Card.Body>
                </a>
            </Card>
            {
                (userIsAdmin ? (
                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Room Admin View</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <p>Colleagues: ...</p>
                            <p>Days: ...</p>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="primary" onClick={handleClose}>
                                OK
                            </Button>
                            <Button variant="primary" onClick={handleRemoveRoom}>
                                Delete
                            </Button>
                        </Modal.Footer>
                    </Modal>
                ) : (<Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Today is the ... day since you started this habit
                            and your rabbit family is still growing.</p>
                        <img src={familyPicture}
                             className="profile-picture-img profile-avatar mt-3"
                             width="350"
                             height="250"
                             border={"3"}
                             alt="family"/>
                        <p></p>
                        <p>Your colleagues: ...</p>
                        There are still 15 days left!
                    </Modal.Body>
                    <Modal.Footer>
                        {/*<Button variant="primary" onClick={handleClose}>
                            Oh Great! (*˘︶˘*)
                        </Button>*/}
                        <BornRabbit
                            item={{playerId: userInfo.id, id: id, isClicked:isClicked, jwt: userInfo.jwt}}
                        />
                        <Button variant="secondary" onClick={handleClose}>
                            I'm scary, cancel please
                        </Button>
                    </Modal.Footer>
                </Modal>))
            }

        </div>
    );
}
