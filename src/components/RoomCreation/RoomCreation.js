import {Button, Card, Col, Container, Form, Modal, Row} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createRoom} from "../../actions/roomAction";

export default function RoomCreation(props) {
    const [show, setShow] = useState(false);

    const userLogin = useSelector(state => state.userLogin)
    const {userInfo} = userLogin

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [finishedAt, setFinishedAt] = useState('')
    const [emails, setEmails] = useState([])

    const [rabbitsForFailure, setRabbitsForFailure] = useState(0)
    const [rabbitsForSuccess, setRabbitsForSuccess] = useState(0)
    const dispatch = useDispatch()


    function handleSave() {
        dispatch(createRoom(title, description, userInfo.id, finishedAt, rabbitsForFailure, rabbitsForSuccess, emails, userInfo.jwt))
        setTitle('')
        setDescription('')
        setFinishedAt('')
        setRabbitsForSuccess(0)
        setRabbitsForFailure(0)
        setEmails('')

        setShow(false)
    }

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <div>
            <Button className="rounded-pill ms-1 mt-5 w-10"
                    variant="outline-dark"
                    type="submit"
                    onClick={handleShow}
            >+</Button>

            <Modal show={show} onHide={handleClose}
                   {...props}
                   size="lg"
                   aria-labelledby="contained-modal-title-vcenter"
                   centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>Creating a new room</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                value={title}
                                onChange={(e) => {
                                    setTitle(e.target.value)
                                }}
                                placeholder="here is the name of your party"
                                autoFocus
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                value={description}
                                onChange={(e) => {
                                    setDescription(e.target.value)
                                }}
                                placeholder="here is the description of your party"
                                autoFocus
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Emails of your friends separate by comma</Form.Label>
                            <Form.Control
                                type="text"
                                value={emails}
                                onChange={(e) => {
                                    const emails = e.target.value;
                                    setEmails(emails.split(','))
                                }}
                                placeholder="emails your friends"
                                autoFocus
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>When will the competition end?</Form.Label>
                            <Form.Control
                                type="date"
                                autoFocus
                                value={finishedAt}
                                onChange={(e) => {
                                    setFinishedAt(e.target.value)
                                }}
                            />
                        </Form.Group>
                        <p className="fs-6 ms-1 fw-light">You and your friends have to go into this room every day and
                            share your successes,
                            for a successful day you are given a certain number of rabbits, and for a failure,
                            you are taken away</p>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Get ... rabbits</Form.Label>
                            <Form.Control
                                value={rabbitsForSuccess}
                                onChange={(e) => {
                                    setRabbitsForSuccess(Number(e.target.value))
                                }}
                                type="number"
                                autoFocus
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Lose ... rabbits</Form.Label>
                            <Form.Control
                                value={rabbitsForFailure}
                                onChange={(e) => {
                                    setRabbitsForFailure(Number(e.target.value))
                                }}
                                type="number"
                                autoFocus
                            />
                        </Form.Group>
                        <p className="fs-6 ms-1 ">Good luck! (*˘︶˘*)♡</p>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Back
                    </Button>
                    <Button variant="primary" onClick={handleSave}>
                        Save
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
