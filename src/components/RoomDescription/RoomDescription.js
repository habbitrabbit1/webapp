import {Card} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import React, { useState, useEffect } from 'react';

export default function RoomDescription(props){
    return(
        <div>
            <LinkContainer to='/' style={{ width: '19rem' }}>
                <Card border="primary" style={{ width: '18rem' }}>
                    <Card.Header></Card.Header>
                    <Card.Body>
                        <Card.Title>Primary Card Title</Card.Title>
                        <Card.Text>
                            Some quick example text to build on the card title and make up the bulk
                            of the card's content.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </LinkContainer>
        </div>
    );
}
