import {Button, Card, Modal} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import React, { useState, useEffect } from 'react';
import ProfilePicture from "../../assets/img/avatar.jpg";
import familyPicture from "../../assets/img/family.jpg";

export default function User(props){
    const [show, setShow] = useState(false);

    const { name, email, createdAt, countOfRabbits} = props.item;

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return(
        <div>
            <Card border="secondary" className="mx-1" style={{ width: '18rem' }}>
                <a style={{ cursor: 'pointer' }} onClick={handleShow}>
                    <Card.Header></Card.Header>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Text>
                            {email}
                        </Card.Text>
                    </Card.Body>
                </a>
            </Card>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Player</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Player since {createdAt}, Rabbits: {countOfRabbits}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={handleClose}>
                        Delete
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        OK
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
