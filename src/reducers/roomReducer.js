import {
    CREATE_ROOM_FAIL,
    CREATE_ROOM_REQUEST,
    CREATE_ROOM_SUCCESS, FETCH_ROOM_FAIL,
    FETCH_ROOM_REQUEST, FETCH_ROOM_SUCCESS
} from "../constants/roomConstants";
import {CONFIRM_ROOM_FAIL, CONFIRM_ROOM_REQUEST, CONFIRM_ROOM_SUCCESS} from "../constants/userConstants";

export const roomCreateReducer = (state = {}, action) => {
    switch (action.type) {
        case CREATE_ROOM_REQUEST:
            return {loading: true}

        case CREATE_ROOM_SUCCESS:
            return {loading: false, userInfo: action.payload}

        case CREATE_ROOM_FAIL:
            return {loading: false, error: action.payload}

        default:
            return state
    }
}

export const roomFetchReducer = (state = {}, action) => {
    switch (action.type) {
        case FETCH_ROOM_REQUEST:
            return {loading: true}

        case FETCH_ROOM_SUCCESS:
            return {loading: false, rooms: action.payload}

        case FETCH_ROOM_FAIL:
            return {loading: false, error: action.payload}

        default:
            return state
    }
}

export const roomConfirmationReducer = (state = {}, action) => {
    switch (action.type) {
        case CONFIRM_ROOM_REQUEST:
            return {loading: true}

        case CONFIRM_ROOM_SUCCESS:
            return {loading: false, rooms: action.payload}

        case CONFIRM_ROOM_FAIL:
            return {loading: false, error: action.payload}

        default:
            return state
    }
}

export const roomDeleteReducer = (state = {}, action) => {
    switch (action.type) {
        case CONFIRM_ROOM_REQUEST:
            return {loading: true}

        case CONFIRM_ROOM_SUCCESS:
            return {loading: false, rooms: action.payload}

        case CONFIRM_ROOM_FAIL:
            return {loading: false, error: action.payload}

        default:
            return state
    }
}
