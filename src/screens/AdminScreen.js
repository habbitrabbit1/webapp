import Navibar from "../components/Navibar/Navibar";
import React, { useState, useEffect } from 'react';
import Profile from "../components/Profile/Profile";
import Admin from "../components/Admin/Admin";

function AdminScreen(){
    return (
        <div>
            <Navibar/>
            <Admin/>
        </div>
    )
}

export default AdminScreen