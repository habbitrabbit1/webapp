import React from 'react'
import Navibar from "../components/Navibar/Navibar";
import HomeContent from "../components/HomeContent/HomeContent";
import robImg from "../assets/img/rob.jpg"
import {Card, Col, Container, Row} from "react-bootstrap";
import helpImg from "../assets/img/help.png";

function HelpScreen() {
    return (
        <div>
            <Navibar />
            <Container fluid>
                <Row>
                    <Col>
                        <div className="all-home-page-text">
                            <h1 className="fw-bolder px-3 fw-bolder fs-1" style={{color: "#0B4654"}}>Rules of our game:</h1>
                            <h1 className="py-3"></h1>
                            <div className="home-page-text">
                                <p className="px-3 fs-4" style={{color: "#0B4654"}}> 🐰 You can create a room where you and your friends can develop good habits.</p>
                                <p className="px-3 fs-4" style={{color: "#0B4654"}}> 🐰 Every day you need to go into the room and check in, then you will receive a rabbit.</p>
                                <p className="px-3 fs-4" style={{color: "#0B4654"}}> 🐰 If suddenly you forgot to enter the room, then the rabbit dies.</p>
                                <p className="px-3 fs-4" style={{color: "#0B4654"}}> 🐰 When the competition ends, you top up your bunny balance.</p>

                            </div>
                        </div>
                    </Col>
                    <Col>
                        <img src={helpImg}
                             className="rounded float-end boy-img"
                             width="500"
                             height="680"
                             alt="..."/>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default HelpScreen