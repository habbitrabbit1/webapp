import React from 'react'
import Navibar from "../components/Navibar/Navibar";
import HomeContent from "../components/HomeContent/HomeContent";
import boyImg from "../assets/img/boy.png"

function HomeScreen() {
  return (
    <div>
        <Navibar />
        <HomeContent/>
    </div>
  )
}

export default HomeScreen