import React, { useState, useEffect } from 'react'
import Navibar from "../components/Navibar/Navibar";
import Rectangle from "../components/Rectangle/Rectangle";
import Login from "../components/Login/Login";
import Background from "../components/Background/Background";


function LoginScreen(){
    return (
        <>
            <Background/>
            <Rectangle/>
            <Navibar />
            <Login/>
       </>
    )
}

export default LoginScreen
