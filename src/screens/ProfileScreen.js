import Navibar from "../components/Navibar/Navibar";
import React, { useState, useEffect } from 'react';
import Profile from "../components/Profile/Profile";

function ProfileScreen(){
    return (
        <div>
            <Navibar />
            <Profile/>
        </div>
    )
}

export default ProfileScreen