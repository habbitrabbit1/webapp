import React, { useState, useEffect } from 'react'
import Background from "../components/Background/Background";
import Rectangle from "../components/Rectangle/Rectangle";
import Navibar from "../components/Navibar/Navibar";
import Register from "../components/Register/Register";


function RegisterScreen(){
    return (
        <>
            <Background/>
            <Rectangle/>
            <Navibar />
            <Register/>
        </>
    )
}

export default RegisterScreen